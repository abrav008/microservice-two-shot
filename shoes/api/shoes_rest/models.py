from django.db import models
from django.urls import reverse


class binVO(models.Model):
    import_href = models.CharField(max_length=50, unique=True)
    closet_name = models.CharField(max_length=100)


class Shoes(models.Model):
    name = models.CharField(max_length=150)
    manufacturer = models.CharField(max_length=50)
    color = models.CharField(max_length=150)
    picture = models.URLField()

    bin = models.ForeignKey(binVO, related_name=("shoes"), on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    # def get_api_url(self):
    #     return reverse("api_show_shoes", kwargs={"pk": self.pk})
