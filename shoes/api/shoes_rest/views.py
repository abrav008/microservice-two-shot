from django.http import JsonResponse
from common.json import ModelEncoder
import json
from .models import Shoes, binVO
from django.views.decorators.http import require_http_methods


class binVODetailEncoder(ModelEncoder):
    model = binVO
    properties = ["import_href", "closet_name"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["name", "id"]
    encoders = {"bin": binVODetailEncoder()}

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["name", "manufacturer", "color", "picture", "bin"]
    encoders = {"bin": binVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = binVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except binVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        shoe = Shoes.objects.get(id=pk)
        shoe.delete()
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

    else:
        try:
            content = json.loads(request.body)
            shoe = Shoes.objects.get(id=pk)

            bin = bin = binVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
            props = ["name", "manufacturer", "color", "picture", "bin", "id"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(shoe, encoder=ShoesDetailEncoder, safe=False)
        except Shoes.DoesNotExist:
            response = JsonResponse({"message", "invalid shoe"})
            response.status_code = 400
            return response
