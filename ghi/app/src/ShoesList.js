import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ShoeColumn({ shoes }) {

    // async function handleClick(event) {
    //     const value  = event.target.value;
    //     console.log(value)
    //     const url = `http://localhost:8080/api/shoes/${value}`
    //     const response = await fetch(url, { method: "DELETE" });

    //     if (response.ok) {
    //         console.log("deleted")
    //         getShoes()
    //     }
    // }
    return (
        <div className='column'>
            {shoes.map(shoe => (
                <div key={shoe.id} className="card">
                    <img src={shoe.picture} alt={shoe.name} />
                    <h4>{shoe.name}</h4>
                    <p>{shoe.color}</p>
                    <p>{shoe.manufacturer}</p>
                    {shoe.bin && <p>{shoe.bin.closet_name}</p>}
                    {/* <button onClick={handleClick} className='btn btn-primary' value={shoe.id} name="button">Delete</button> */}
                </div>
            ))}
        </div>
    );
}

function Shoes({ shoes }) {
    const [shoeColumns, setShoeColumns] = useState([]);

    useEffect(() => {
        async function getShoeDetails() {
            try {
                const requests = [];
                for (let shoe of shoes) {
                    const Url = `http://localhost:8080/api/shoes/${shoe.id}`;
                    requests.push(fetch(Url));
                }
                const responses = await Promise.all(requests);
                const shoeColumns = [[], [], []];
                let i = 0;
                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json();
                        shoeColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(shoeResponse);
                    }
                }

                setShoeColumns(shoeColumns)
            } catch (e) {
                console.error(e);
            }
        }
        getShoeDetails();
    }, [shoes])

    return (
        <>
            {/* Your header JSX */}
            <div className='container'>
                <h2>Shoes</h2>
                <div className="row">
                    {shoeColumns.map((shoeList, index) => {
                        return shoeList.map(shoe => (
                            <ShoeColumn key={shoe.id} shoes={shoeList} />
                        ));
                    })}
                </div>
            </div>
        </>
    );
}
export default Shoes
