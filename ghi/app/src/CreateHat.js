import { useEffect, useState } from "react"

function CreateHat({ getHats }){
    const [locations, setLocations] = useState([])
    const [fabric, setFabric] = useState('')
    const [styleName, setStyleName] = useState('')
    const [color, setColor] = useState('')
    const [hatUrl, setHatUrl] = useState('')
    const [location, setLocation] = useState('')

    async function getLocation(){
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setLocations(data.locations);
        } else {
            console.log("response failed")
        }
    }

    useEffect(() => {
        getLocation();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault()
        const data = {
            fabric,
            style_name: styleName,
            color,
            hat_url: hatUrl,
            location,
        };

        const url = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setFabric('')
            setStyleName('')
            setColor('')
            setHatUrl('')
            setLocation('')
            getHats()
        } else {
            console.log("help me")
        }
    }

    function handleChangeFabric(event) {
        const { value } = event.target;
        setFabric(value);
      }

    function handleChangeStyleName(event) {
        const { value } = event.target;
        setStyleName(value);
    }

    function handleChangeColor(event) {
        const { value } = event.target;
        setColor(value);
    }

    function handleChangeHatUrl(event) {
        const { value } = event.target;
        setHatUrl(value);
    }

    function handleChangeLocation(event) {
        const { value } = event.target;
        setLocation(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Put away hat</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input value={styleName} onChange={handleChangeStyleName} placeholder="styleName" required type="text" name="style_name" id="style_name" className="form-control" />
                  <label htmlFor="styleName">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={fabric} onChange={handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="ends">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={hatUrl} onChange={handleChangeHatUrl} placeholder="Hat URL" required type="url" name="hat_url" id="hat_url" className="form-control" />
                  <label htmlFor="hatUrl">Hat URL</label>
                </div>
                <div className="mb-3">
                  <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.href}>{location.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default CreateHat
