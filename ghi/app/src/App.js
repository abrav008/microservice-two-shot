import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList'
import ShoesForm from './ShoesForm'

import Hats from './hats';
import CreateHat from './CreateHat';

function App() {
  const[ shoes, setShoes ] = useState([])

  async function getShoes() {
    const response = await fetch("http://localhost:8080/api/shoes/")
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An error occurred while fetching data')
    }
  }

  useEffect(() => {
    getShoes();
  }, [])



  const[ hats, setHats ] = useState([]);

  async function getHats() {
    const response = await fetch("http://localhost:8090/api/hats/")
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  useEffect(() => {
    getHats();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={shoes} getShoes = {getShoes}/>} />
            <Route path="new" element={<ShoesForm getShoes = {getShoes} />} />
          </Route>
          <Route path="hats">
            <Route index element={<Hats hats={hats} getHats = {getHats}/>} />
            <Route path="new" element={<CreateHat getHats = {getHats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
