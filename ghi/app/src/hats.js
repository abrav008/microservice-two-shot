import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';



function HatColumn(props, { getHats }) {

    async function handleClick(event) {
        const value  = event.target.value;
        console.log(value)
        const url = `http://localhost:8090/api/hats/${value}`
        const response = await fetch(url, { method: "DELETE" });

        if (response.ok) {
            console.log("deleted")
            getHats()
        }
    }

    return (
        <div className='col'>
            {props.list.map (hat => {
                return (
                    <div key={hat.id} className="card mb-3 shadow">
                        <img src={hat.hat_url} className="card-img-top" alt= "" />
                        <div className="card-body">
                            <h5 className="card-title">{hat.style_name}</h5>
                            <p className='card-text'>{hat.color}</p>
                            <p className="card-text">{hat.fabric}</p>
                            <p className="card-text-bold">{hat.location.closet_name}</p>
                            <button onClick={handleClick} className='btn btn-primary' value={hat.id} name="button">Delete</button>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}



function Hats({hats}) {
    const [hatColumns, setHatColumns] = useState([]);

    useEffect(() => {
        async function getHatDetails() {
            try {
                const requests = [];
                for (let hat of hats) {
                    const detailUrl = `http://localhost:8090/api/hats/${hat.id}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const hatColumns = [[], [], []];

                let i = 0;
                for (const hatResponse of responses) {
                    if(hatResponse.ok) {
                        const details = await hatResponse.json();
                        hatColumns[i].push(details);
                        i = i +1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }

                setHatColumns(hatColumns)
            } catch(e) {
                console.error(e);
            }
        }
        getHatDetails();
    }, [hats])

    return(
        <>
        <div className="px-4 py-5 my-5 mt-2 text-center bg-info">
        <h1 className="display-5 fw-bold">Hat collection</h1>
            <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
                Come find your hats buddy
            </p>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                    <Link to="new" className="btn btn-primary btn-lg px-4 gap-3">Store your hat</Link>
                </div>
            </div>
        </div>
            <div className='container'>
                <h2>Hats</h2>
                <div className="row">
                    {hatColumns.map((hatList, index) => {
                        return (
                            <HatColumn key={index} list={hatList} />
                        );
                    })}
                </div>
            </div>
        </>
    );
}
export default Hats;
