import React, { useEffect, useState } from 'react';

function ShoeForm({ getShoes }) {
    const [bins, setBins] = useState([])
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');

    async function fetchBin() {
        const binUrl = 'http://localhost:8100/api/bins/';

        const response = await fetch(binUrl);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }


    useEffect(() => {
        fetchBin();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name,
            manufacturer,
            color,
            picture,
            bin,
        }


        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setName('');
            setManufacturer('');
            setColor('');
            setPicture('');
            setBin('');
            getShoes()
        } else {
            console.log("two thumbs down")
        }
    }
    function handleSetName(event) {
        const { value } = event.target;
        setName(value);
    }

    function handleSetManufacturer(event) {
        const { value } = event.target;
        setManufacturer(value);
    }

    function handleSetColor(event) {
        const { value } = event.target;
        setColor(value);
    }

    function handleSetPicture(event) {
        const { value } = event.target;
        setPicture(value);
    }

    function handleSetBins(event) {
        const { value } = event.target;
        setBin(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new shoe trackers!</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleSetName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleSetManufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleSetColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={picture} onChange={handleSetPicture} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
                            <label htmlFor="picture">Picture</label>
                        </div>
                        <div value={bin} className="form-floating mb-3">
                            <select value={bin} onChange={handleSetBins} required name="bin" id="bin" className="form-select">
                                <option value="">Select a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
