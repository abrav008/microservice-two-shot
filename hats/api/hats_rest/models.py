from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    hat_url = models.URLField(null=True)

    location= models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )
